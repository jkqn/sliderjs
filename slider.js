/*

http://www.louney.com/home/sliderjs
      ________
<----(sliderJS)---->

created by Jakob Kuen
=====================
  A small js script
   for html5 form
 slider inputs using
  only HTML5 and JS

Also compatible with
     PHP/AJAX

Usage:

Include this in your head:
<script src="path/to/slider.js"></script>
<link rel="stylesheet" href="path/to/slider.css">

<div class="slider" 
min="[minimum amount for slider]" 
max="[maximum amount for slider]"
width="[width for slider]"
name="[name for slider(PHP/AJAX forms)]"></div>

Classes for CSS:
.slider_rail: main slider element
.slider_button: slider button
.slider_input: slider button

Use this however you want, but keep this license here
*/

window.onload = function(){
	sliders = document.getElementsByClassName("slider");
	for(var i = 0; i < sliders.length; i++){
		sliders[i].min = sliders[i].getAttribute("min");
		sliders[i].max = sliders[i].getAttribute("max");
		sliders[i].width = sliders[i].getAttribute("width");
		sliders[i].name = sliders[i].getAttribute("name");
		sliders[i].style.width = sliders[i].width;
		button = document.createElement("div");
		button.className = "slider_button";
		
		button.onmousedown = function(event){
			this.sliding = true;
			document.getElementsByTagName("body")[0].style.webkitTouchCallout = "none";
			document.getElementsByTagName("body")[0].style.webkitUserSelect = "none";
			document.getElementsByTagName("body")[0].style.khtmlUserSelect = "none";
			document.getElementsByTagName("body")[0].style.mozUserSelect = "none";
			document.getElementsByTagName("body")[0].style.msUserSlecet = "none";
			document.getElementsByTagName("body")[0].style.userSelect = "none";
		}
		sliders[i].appendChild(button);
		input = document.createElement("input");
		input.type = "text";
		input.className = "slider_input";
		var left = parseInt(sliders[i].width) + 10;
		input.style.left = left +  "px";
		input.value = sliders[i].min;
		input.name = sliders[i].name;
		sliders[i].appendChild(input);
	}
}

document.onmouseup = function(){
	for(var i = 0; i < sliders.length; i++){
		sliders[i].firstChild.sliding = false;
	}
	document.getElementsByTagName("body")[0].style.webkitTouchCallout = "initial";
	document.getElementsByTagName("body")[0].style.webkitUserSelect = "initial";
	document.getElementsByTagName("body")[0].style.khtmlUserSelect = "initial";
	document.getElementsByTagName("body")[0].style.mozUserSelect = "initial";
	document.getElementsByTagName("body")[0].style.msUserSlecet = "initial";
	document.getElementsByTagName("body")[0].style.userSelect = "initial";
}

document.onmousemove = function(event){
	for(var i = 0; i < sliders.length; i++){
		if(sliders[i].firstChild.sliding){
			var x;
			var e = sliders[i];
			var left = 0;
			while(e.tagName != "BODY") {
				left += e.offsetLeft;
				e = e.offsetParent;
			}
			x = left;

			var X = event.clientX - x;
			if(X < 0){
				sliders[i].firstChild.style.left = 0;
			}else if(X > sliders[i].width){
				sliders[i].firstChild.style.left = sliders[i].width;
			}else{
				sliders[i].firstChild.style.left = X;
			}
			var number = (parseInt(sliders[i].firstChild.style.left) / parseInt(sliders[i].width) * parseInt((sliders[i].max - sliders[i].min)) + parseInt(sliders[i].min));
			sliders[i].childNodes[1].value = Math.floor(number);
		}
	}
}